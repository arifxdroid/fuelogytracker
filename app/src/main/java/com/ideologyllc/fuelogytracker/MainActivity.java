package com.ideologyllc.fuelogytracker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ideologyllc.fuelogytracker.location.LocationTraker;
import com.thingworx.common.RESTAPIConstants;
import com.thingworx.communications.client.AndroidConnectedThingClient;
import com.thingworx.communications.client.ClientConfigurator;
import com.thingworx.relationships.RelationshipTypes;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.primitives.LocationPrimitive;
import com.thingworx.types.primitives.StringPrimitive;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private String uri = "ws://23.228.149.221:80/Thingworx/WS";
    private String appKey = "0001f50b-fc24-4649-b7df-8aba824bcad6";
    protected AndroidConnectedThingClient client;
    protected ClientConfigurator config;
    private ProgressDialog connectionProgress;
    Context context;

    private StringPrimitive timePrimitive;
    private StringPrimitive latitudePrimitive;
    private StringPrimitive longitudePrimitive;
    private StringPrimitive remarksPrimitive;
    private LocationPrimitive locationPrimitive;

    private String newLatitude;
    private String newLongitude;
    private String currentTime;

    private Double latitude;
    private Double longitude;

    private TextView textLocation;
    private EditText etRemarks;
    private Button btnSend;
    private Spinner spinner;

    private Geocoder geocoder;
    private List<Address> addresses;

    private String address;
    private String city;
    private String state;
    private  String country;
    private String postalCode;
    private String knownName;
    private String currentStatus;

    private ArrayList<String> statusList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        connectingToCloud();
        initialization();
        getAddress();
        textLocation.setText(address+", "+city + ", " + state + ", " + country + ", " + postalCode + ".");

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etRemarks.getText().toString() != null){
                    sendDataToCloud();
//                    Toast.makeText(getApplicationContext(), currentTime, Toast.LENGTH_LONG).show();
                }else {
//                    Toast.makeText(getApplicationContext(), currentTime, Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Write your remarks", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void initialization(){

        context = this;
        textLocation = (TextView)findViewById(R.id.textLocation);
        etRemarks = (EditText)findViewById(R.id.etRemarks);
        btnSend = (Button)findViewById(R.id.btnSend);
        spinner = (Spinner)findViewById(R.id.spinner);
        getCurrentTime();
        getMyLocation();

        statusList = new ArrayList<>();
        statusList.add("DRIVING");
        statusList.add("NOTDRIVING");
        statusList.add("OFFDUTY");
        statusList.add("SLEEPING");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, statusList);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentStatus = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                currentStatus = "NOTDRIVING";
            }
        });

    }

    private void connectingToCloud() {
        config = new ClientConfigurator();
        config.setUri(uri);
        config.setReconnectInterval(15);
        config.getSecurityClaims().addClaim(RESTAPIConstants.PARAM_APPKEY, appKey);
        config.ignoreSSLErrors(true);
        config.setConnectTimeout(10000);

        try {
            client = new AndroidConnectedThingClient(config);
            client.start();
            connectionProgress = ProgressDialog.show(MainActivity.this, null, "Connecting Please wait...");

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    boolean isConnected = client.getEndpoint().isConnected();
                    if (isConnected) {
                        String state = "connected";
                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", state);
                        msg.setData(b);
                        handler.sendMessage(msg);
                    }else {

                        String state = "notConnected";
                        Message msg = Message.obtain();
                        Bundle b = new Bundle();
                        b.putString("state", state);
                        msg.setData(b);
                        handler.sendMessage(msg);
                    }
                }
            }).start();

        } catch (Exception e) {
            e.printStackTrace();

            String state = "notConnected";
            Message msg = Message.obtain();
            Bundle b = new Bundle();
            b.putString("state", state);
            msg.setData(b);
            handler.sendMessage(msg);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String state;
            state = msg.getData().getString("state");

            switch (state) {

                case "connected":
                    connectionProgress.dismiss();
                    break;

                case "notConnected":
                    connectionProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Problem with connection.", Toast.LENGTH_LONG).show();
                    //restartActivity();
                    break;

                default:
                    connectionProgress.dismiss();
                    restartActivity();
                    break;
            }
        }
    };

    private void sendDataToCloud(){

        connectionProgress = ProgressDialog.show(MainActivity.this, null, "Sending data. Please wait...");
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ValueCollection params = new ValueCollection();
                params.put("StartTime", timePrimitive);
                params.put("DrivingState", new StringPrimitive(currentStatus));
                params.put("latitude", latitudePrimitive);
                params.put("longitude", longitudePrimitive);
                params.put("remarks", new StringPrimitive(etRemarks.getText().toString()));
                params.put("location", locationPrimitive);

                try {
                    client.invokeService(RelationshipTypes.ThingworxEntityTypes.Things, "FuelogyData", "addDriverProperty", params, 5000);

                    Message msg = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("state", "true");
                    msg.setData(b);
                    handlerSendingData.sendMessage(msg);

                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("state", "false");
                    msg.setData(b);
                    handlerSendingData.sendMessage(msg);
                }
            }
        }).start();
    }

    Handler handlerSendingData = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String state;
            state = msg.getData().getString("state");

            switch (state) {
                case "true":
                    connectionProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Sending Successful", Toast.LENGTH_LONG).show();
                    break;
                case "false":
                    connectionProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Sending Failed", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    //Getting Current Location
    private void getMyLocation() {
        LocationTraker traker = new LocationTraker(context);

        if (traker.getState()) {
            latitude = traker.getlat();
            longitude = traker.getLon();

            newLatitude = Double.toString(latitude);
            newLongitude = Double.toString(longitude);
            latitudePrimitive = new StringPrimitive(newLatitude);
            longitudePrimitive = new StringPrimitive(newLongitude);
            locationPrimitive = new LocationPrimitive(latitude, longitude, 0.0);

        }
    }

    private void getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("h.mma");
        currentTime = sdf.format(new Date());
        timePrimitive = new StringPrimitive(currentTime);
    }

    private void getAddress(){

        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses.size() > 0){

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getFeatureName();
        }

    }

    //Restart Activity
    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            client.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
