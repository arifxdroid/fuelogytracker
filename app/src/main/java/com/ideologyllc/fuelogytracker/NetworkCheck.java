package com.ideologyllc.fuelogytracker;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.ideologyllc.fuelogytracker.connectivity.NetworkUtil;

import java.lang.reflect.InvocationTargetException;

public class NetworkCheck extends AppCompatActivity {

    private NetworkUtil networkUtil;
    private ProgressDialog progressDialog;
    private String medPodId = "null";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_check);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().hide();

        networkUtil = new NetworkUtil(getApplicationContext());
        if (networkUtil.isConnectingToInternet(getApplicationContext())) {
            String netState = networkUtil.getConnectivityStatusString(getApplicationContext());
            Toast.makeText(this, "Connected and " + netState, Toast.LENGTH_SHORT).show();
            afterConnection();

        } else {
            Toast.makeText(this, "Not Connected", Toast.LENGTH_SHORT).show();
            networkOptionDialogue();
        }
    }

    public void networkOptionDialogue() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.network_connection);
        dialog.setTitle("Network Connection");

        // setting up custom dialog components - text, image and button
        Switch switchWifi = (Switch) dialog.findViewById(R.id.switchWifi);
        Switch switchMobile = (Switch) dialog.findViewById(R.id.switchMobile);

        switchWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    progressDialog = ProgressDialog.show(NetworkCheck.this, "WiFi", "Connecting...");
                    networkUtil.toggleWiFi(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Thread.sleep(10000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (networkUtil.isConnectingToInternet(getApplicationContext())) {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "connected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            } else {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "notConnected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            }
                        }
                    }).start();

                } else {
                    Toast.makeText(NetworkCheck.this, "Off", Toast.LENGTH_LONG).show();
                }
            }
        });


        switchMobile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    progressDialog = ProgressDialog.show(NetworkCheck.this, "Mobile Data", "Connecting...");
                    try {
                        networkUtil.toggleMobileData(true);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Thread.sleep(12000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (networkUtil.isConnectingToInternet(getApplicationContext())) {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "connected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            } else {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "notConnected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            }
                        }
                    }).start();

                } else {
                    Toast.makeText(NetworkCheck.this, "Off", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Dialog closing button
        Button buttonClose = (Button) dialog.findViewById(R.id.buttonClose);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                restartActivity();
            }
        });
        dialog.show();
    }

    //Handler
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String netState;
            netState = msg.getData().getString("state");
            switch (netState) {

                case "connected":
                    progressDialog.dismiss();
                    Toast.makeText(NetworkCheck.this, "connected", Toast.LENGTH_LONG).show();
                    afterConnection();
                    break;
                case "notConnected":
                    progressDialog.dismiss();
                    Toast.makeText(NetworkCheck.this, "Connection problem", Toast.LENGTH_LONG).show();
                    restartActivity();
                    break;
            }
        }
    };

    //Restart Activity
    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }


    // Doning work after making internet connection
    private void afterConnection() {
        Intent i = new Intent(NetworkCheck.this, MainActivity.class);
        startActivity(i);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();

    }


}
